/**
 * Copyright 2016-2018 Eloy García Almadén <eloy.garcia.pca@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.estoes.wallpaperDownloader.changer;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import org.apache.log4j.Logger;
import es.estoes.wallpaperDownloader.util.ImageFilenameFilter;
import es.estoes.wallpaperDownloader.util.PreferencesManager;
import es.estoes.wallpaperDownloader.util.WDUtilities;

/**
 * Abstract class which defines a wallpaper changer.
 * @author egarcia
 *
 */
public abstract class WallpaperChanger {
	
	// Constants
	protected static final Logger LOG = Logger.getLogger(WallpaperChanger.class);
	
	// Attributes
	/**
	 * All the wallpapers available in the system for changing the background.
	 */
	private LinkedList<String> wallpapers;
	
	// Getters & Setters
	/**
	 * Gets wallpapers.
	 * @return the wallpapers
	 */
	public LinkedList<String> getWallpapers() {
		return this.wallpapers;
	}

	/**
	 * Sets wallpapers.
	 * @param wallpapers the wallpapers to set
	 */
	public void setWallpapers(LinkedList<String> wallpapers) {
		this.wallpapers = wallpapers;
	}

	// Methods
	/**
	 * Constructor
	 */
	public WallpaperChanger () {
		if (LOG.isInfoEnabled()) {
			LOG.info("Instantiating wallpaper changer...");
		}
		this.wallpapers = null;
	}
	
	/**
	 * Sets Desktop's wallpaper.
	 * @param wallpaperPath path of the wallpaper to be set
	 */
	public abstract void setWallpaper(String wallpaperPath);

	/**
	 * Checks if the desktop environment allows to change the wallpaper.
	 * @return boolean
	 */
	public abstract boolean isWallpaperChangeable();

	/**
	 * Sets a random wallpaper.
	 */
	public void setRandomWallpaper() {
		PreferencesManager prefm = PreferencesManager.getInstance();
		List<File> randomWallpapers = new ArrayList<File>();
		String changerFoldersProperty = prefm.getPreference("application-changer-folder");
		String[] changerFolders = changerFoldersProperty.split(";");
		for (int i = 0; i < changerFolders.length; i ++) {
			File randomWallpaper = WDUtilities.pickRandomImage(changerFolders[i]);
			if (randomWallpaper != null) {
				randomWallpapers.add(randomWallpaper);				
			}
		}
		if (randomWallpapers.size() > 0) {
			Random generator = new Random();
			int index = generator.nextInt(randomWallpapers.size());
			this.setWallpaper(randomWallpapers.get(index).getAbsolutePath());
			if (LOG.isInfoEnabled()) {
				LOG.info("Setting random wallpaper: " + randomWallpapers.get(index).getAbsolutePath());
			}
		}
	}

	/**
	 * Sets a sorted wallpaper.
	 */
	public void setSortedWallpaper() {
		PreferencesManager prefm = PreferencesManager.getInstance();
		String changerMode = prefm.getPreference("application-changer-mode");

		if (this.getWallpapers() == null) {
			// If there is not a list of available wallpapers, then it will
			// be created. It will be used a LinkedList in order to use it
			// as a FIFO or LIFO stack
			LinkedList<String> availableWallpapers = new LinkedList<String>();
			if (LOG.isInfoEnabled()) {
				LOG.info("Retrieving all the available wallpapers in the system and sorting them in order to pick the right one. Please wait...");
			}
			String changerFoldersProperty = prefm.getPreference("application-changer-folder");
			String[] changerFolders = changerFoldersProperty.split(";");
			// Retrieving all the available wallpapers within the directories defined by the user
			File[] availableWallpapersArray = {};
			for (int index = 0; index < changerFolders.length; index ++) {
				if (LOG.isInfoEnabled()) {
					LOG.info("Getting all the images in " + changerFolders[index]);
				}
				if (Files.exists(Paths.get(changerFolders[index]))) {
					File directory = new File(changerFolders[index]);
					FilenameFilter imageFilenameFilter = new ImageFilenameFilter();
					File[] images = directory.listFiles(imageFilenameFilter);
					availableWallpapersArray = WDUtilities.append(images, availableWallpapersArray);				
				} else {
					if (LOG.isInfoEnabled()) {
						LOG.error("Directory " + changerFolders[index] + " doesn't exist");
					}
				}
			}
			
			// Sorting all the wallpapers
			if (changerMode.equals("1")) {
				// From oldest to newest
				Arrays.sort(availableWallpapersArray, Comparator.comparingLong(File::lastModified));
			} else if (changerMode.equals("2")) {
				// From newest to oldest
				Arrays.sort(availableWallpapersArray, Comparator.comparingLong(File::lastModified).reversed());
			}
			
			// Building availableWallpapers already sorted
			for (int index = 0; index < availableWallpapersArray.length; index ++) {
				availableWallpapers.add(availableWallpapersArray[index].getAbsolutePath());
			}
			
			// Retrieving the last wallpaper set
			String lastWallpaperSetPath = prefm.getPreference("application-changer-mode-sort-last-wallpaper");
			
			// Removing all the wallpapers that have been already set if it is necessary
			if (!lastWallpaperSetPath.equals(PreferencesManager.DEFAULT_VALUE)) {
				while (!lastWallpaperSetPath.equals(availableWallpapers.peekFirst())) {
					availableWallpapers.pollFirst();
				}
				// Now, the last wallpaper set is removed too
				availableWallpapers.pollFirst();
			}
			this.setWallpapers(availableWallpapers);
		}
		
		// The next sorted wallpaper is set
		String nextWallpaperPath = this.getWallpapers().peekFirst();
		if (nextWallpaperPath != null) {
			prefm.setPreference("application-changer-mode-sort-last-wallpaper", nextWallpaperPath);
			this.getWallpapers().pollFirst();
			this.setWallpaper(nextWallpaperPath);
		} else {
			// The stack of wallpapers is empty so the process starts again from the beginning
			prefm.setPreference("application-changer-mode-sort-last-wallpaper", PreferencesManager.DEFAULT_VALUE);
			this.setWallpapers(null);
			this.setSortedWallpaper();
		}
	}
}
